from django.shortcuts import render
from django.contrib.auth.models import User, Group
from music_shop.shop_api.models import (
    Genre, MediaType, Artist, Track, Album)
from rest_framework import viewsets
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from music_shop.shop_api.serializers import (
    UserSerializer, GroupSerializer,
    GenreSerializer, MediaTypeSerializer,
    ArtistSerializer, TrackSerializer, 
    AlbumSerializer)



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GenreViewSet(viewsets.ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class MediaTypeViewSet(viewsets.ModelViewSet):
    queryset = MediaType.objects.all()
    serializer_class = MediaTypeSerializer


class ArtistViewSet(viewsets.ModelViewSet):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer


class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer


class AlbumViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer


class ArtistList(APIView):
    """
    List all Artist, or create a new artist.
    """
    def get(self, request, format=None):
        artist = Artist.objects.all()
        serializer = ArtistSerializer(artist, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ArtistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ArtistDetail(APIView):
    """
    Retrieve, update or delete a artist instance.
    """
    def get_object(self, pk):
        try:
            return Artist.objects.get(pk=pk)
        except Artist.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        artist = self.get_object(pk)
        serializer = ArtistSerializer(artist)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        artist = self.get_object(pk)
        serializer = ArtistSerializer(artist, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        artist = self.get_object(pk)
        artist.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


## Using generics.ListCreateAPIView
class AlbumsByArtistList(generics.ListCreateAPIView):
    """
    List all Album of an artist, or create a new artist.
    Using generics views
    """
    def get_queryset(self):
        queryset = Album.objects.filter(artist=self.kwargs["pk"])
        return queryset
    serializer_class = AlbumSerializer


class TracksByArtistList(generics.ListCreateAPIView):
    """
    List all Tracks of an artist, or create a new artist.
    Using generics views
    """
    def get_queryset(self):
        queryset = Track.objects.filter(artist=self.kwargs["pk"])
        return queryset
    serializer_class = TrackSerializer

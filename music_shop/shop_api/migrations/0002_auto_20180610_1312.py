# Generated by Django 2.0.6 on 2018-06-10 13:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop_api', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='track',
            old_name='durartion',
            new_name='duration',
        ),
    ]

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = 'profile'
        verbose_name_plural = 'profiles'

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
    

class MediaType(models.Model):
    name = models.CharField(max_length=255, blank=True, default='')

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=255, blank=True, default='')
    def __str__(self):
        return self.name


class Artist(models.Model):
    name = models.CharField(max_length=255, blank=True, default='')

    def __str__(self):
        return self.name


class Album(models.Model):
    title = models.CharField(max_length=255, blank=True, default='')
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE,)
    year = models.IntegerField(blank=True, default=0, null=True)

    def __str__(self):
        return self.title


class Track(models.Model):
    title = models.CharField(max_length=255, blank=True, default='')
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE,)
    album = models.ForeignKey(Album, on_delete=models.CASCADE,)
    media_type = models.ForeignKey(MediaType, on_delete=models.CASCADE,)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE,)
    composer = models.CharField(max_length=255, blank=True, default='')
    duration = models.CharField(max_length=255, blank=True, default='')
    year = models.IntegerField(blank=True, default=0, null=True)

    def __str__(self):
        return self.title


class PlayList(models.Model):
    name = models.CharField(max_length=255, blank=True, default='')
    created = models.DateTimeField(auto_now_add=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True)
    is_public = models.BooleanField(default=False)
    tracks = models.ManyToManyField(Track, through='PlayListTracks')

    def __str__(self):
        return self.name


class PlayListTracks(models.Model):
    track = models.ForeignKey(Track, on_delete=models.CASCADE,)
    play_list = models.ForeignKey(PlayList, on_delete=models.CASCADE)

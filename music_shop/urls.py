"""music_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from music_shop.shop_api import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'media_type', views.MediaTypeViewSet)
router.register(r'genre', views.GenreViewSet)
router.register(r'artist', views.ArtistViewSet)
router.register(r'track', views.TrackViewSet)
router.register(r'album', views.AlbumViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^artist-list/$', views.ArtistList.as_view()),
    url(r'^artist-detail/(?P<pk>[0-9]+)/$', views.ArtistDetail.as_view()),
    url(r'^artist-albums/(?P<pk>[0-9]+)/$', views.AlbumsByArtistList.as_view()),
    url(r'^artist-tracks/(?P<pk>[0-9]+)/$', views.TracksByArtistList.as_view())

]

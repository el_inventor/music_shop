# Install pip3 on linux as root, use sudo in Ubuntu
apt-get install python3-pip

# Create the project directory
mkdir ms_shop
cd ms_shop

# Create a virtualenv to isolate our package dependencies locally
virtualenv --python=`which python3` env
source env/bin/activate  # On Windows use `env\Scripts\activate`

# Install Django and Django REST framework into the virtualenv
pip3 install django
pip3 install djangorestframework
### Replace this for requirementents.txt

# Clone project
# https://gitlab.com/el_inventor/music_shop.git
# or git@gitlab.com:el_inventor/music_shop.git


# The project tree now should be like

../ms_shop/
├── env
├── manage.py
├── music_shop
└── README.md


https://docs.djangoproject.com/en/2.0/intro/contributing/
http://www.sqlitetutorial.net/sqlite-sample-database/